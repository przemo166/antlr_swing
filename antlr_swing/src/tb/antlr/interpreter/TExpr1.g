tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());})*
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = Math.floorMod($e1.out,$e2.out);}
        | ^(POW   e1=expr e2=expr) {$out=1; for(int i=0;i<$e2.out;i++){$out*=$e1.out;}}
        | ^(PODST i1=ID   e2=expr)
        | INT                      {$out = getInt($INT.text);}
        ; 
